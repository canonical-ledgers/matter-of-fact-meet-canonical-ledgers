Canonical Ledgers works with the other Authority Node Operators to stand up a
robust and decentralized network for Factom’s M3 codebase and participates in
organizing and developing the Factom community and ecosystem. They have
published code for a Discord Factoid tipbot, and an add-on for the Factomize
forum that secures posts with Factom. They contribute to node monitoring
systems, the Factom codebase, and documentation of best practices for Factom
server architecture. Finally, they are developing a web store for purchasing
Entry Credits, the non-transferable token used to pay for entries to the Factom
blockchain. Directly purchasing Entry Credits with USD, allows businesses to
avoid the tax implications of first purchasing a cryptocurrency like Factoids.

> We help integrate blockchain based data authentication and validation into a
> company's workflow.

## Who is Canonical Ledgers?
Canonical Ledgers is a small team of developer/engineer/entrepreneurs who are
passionate about making distributed ledgers work in the real world. Their
vision is to build better user experiences of blockchain ledgers for high value
industry applications. Canonical Ledgers views running an Authority Server as
direct way of supporting Factom that allows them to pursue Factom integration
development opportunities. They’ve been involved in the blockchain space for a
number of years and are excited to be taking the next step in making it their
full time careers.

## Canonical Ledgers project/s timeline
1. Canonical Ledgers just completed an add-on for the Factomize forum that
   hashes everyone’s posts onto the Factom blockchain. This is currently in
closed beta testing and is expected to be deployed on factomize.com within a
month.
2. Canonical Ledgers is investigating hardware approaches to authentication of
   real world goods on the blockchain. They are investigating the technology
and the market opportunities. Fake products present a major issue in niche
designer markets where people pay a premium for a genuine product. There is
currently no solution for consumers to verify they’re getting genuine product.
Canonical Ledgers is interested in finding hardware solutions that tie the real
world into the Factom blockchain.
3. Canonical Ledgers expects to be officially onboarded as an Authority Node on
   mainnet in the next couple weeks.


## How does Canonical Ledgers further the Factom protocol?
Canonical Ledgers, represented by Samuel Vanderwaal (@archaeopteryx#7615 on the
Factom Discord server), is a guide in the Factom community. Being a guide
involves contributing to governance discussions as well as working on
documentation, facilitating meetings, votes, decisions, group conversations,
and generally doing whatever needs to be done to keep the Factom community
healthily moving forward.

The projects that Canonical Ledgers is working on, like the Factomize forum
add-on, demonstrate the feasibility of integrating blockchain security into an
application. The more applications built on top of Factom, the greater the
demand for Entry Credits and Factoids.

## What industries does Canonical Ledgers believe the Factom protocol could serve best?
Canonical Ledgers is excited to see what develops more in mortgage and loan
industry, these industries have massive potential for usage of entry credits.
They’re also very interested in ways to tie real world objects to an identity
on Factom to help prevent forgeries and establish brand authenticity. This
could apply to many areas from supply chains to fine art and high end brands.

## Authority Set application process interview questions
### How did you find the process of the Authority Set application?
Honestly, it felt rushed and there is room for improvement. Overall, the
process was sufficient and there was sufficient guidance from the Guides, but
there were a lot of moving parts with getting M3 code out and it sometimes felt
rushed and disorganized. It was a pretty comprehensive set of questions which
detailed nicely the different aspects of the team, but we feel like it was a
bit of a scramble for everybody. A lot of uncertainty at times if we would get
elected and how the votes would turn out. In the end, a good set of teams where
elected of a high pedigree and the Guides did a great job with the objective
and harder to quantify objective factors.Everyone involved that is
participating is competent and sincere in their efforts – we really have a
wealth of talented people and teams. If that is a marker for success, the
application process went great.

### What areas of the application process do you think need amending and why?
When you use server/hardware requirements as a grading criteria it creates an
incentive to just pick the best hardware or the biggest hard drive, simply to
get top scores. I think that created a little bit of a game to max out those
stats for higher grades. I believe the goal was to create a set of objective
quantifiable criteria. Personally, I feel that subjectivity is okay and leave
room for human discretion. For example, the storage of free space for the
Factom database. This didn’t make sense to us so instead of selecting the
highest number for the best score, we ended up writing a paragraph in the other
category about how we use ZFS to allow for expanding our storage dynamically
without interruptions. Although that gives us infinite scalability, it’s hard
to get lumped into a box or multiple choice, the reality is it doesn’t fit the
real world use case. Instead of using Google forms, if you could establish
sections that you want included and give an outline or template and let people
write it, submit in PDFs instead of a Google form that resulted in an
incomprehensible filing cabinet for the applications.

### What has been the most difficult aspect of the process?
Just writing all the copy, we would rather be writing code than writing words.

### What has been the most enjoyable aspect of the process?
Getting selected was obviously very gratifying. It was extremely valuable to go
through that process that made us think through a number of aspects of our
business and our infrastructure. Although a little stressful, we enjoyed
planning out and implementing the network architecture.

### What would you have done differently in hindsight?
Start the general process sooner, we were waiting for a more complete scope of
what was expected before we started compiling info. Starting sooner would’ve
given us more time to polish it.

### Any recommendations for teams that are applying to join the authority set?
Join the testnet! That is the best way to get onboarded and learn the ins and
outs and provides valuable information and experience with the community.

> Canonical means standardized or truth version of something. Ledgers refers to
> distributed databases. Our goal is to tie users records to a distributed
> source of truth.
